import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreetting = "สวัสดี";
String chinaGreeting = "你好";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
              });
            },
                icon: Icon(Icons.refresh)),
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                thaiGreetting:englishGreeting;
              });
            },
                icon: Icon(Icons.delivery_dining )),
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                chinaGreeting:englishGreeting;
              });
            },
                icon: Icon(Icons.forest_rounded ))

          ],
        ),
        body: Center(
          child: Text
            (displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
